﻿using System;
using System.IO;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Labs;
using Xamarin.Forms.Labs.Services;

namespace HypervibeInterfaceApp
{
	public class App
	{
		private static RootGridPage m_RootGridPage = null;

		public static Page GetMainPage ()
		{	
			// access the Program Data Singlton to fill ProgramData from the xlsx files
			ProgramData programData = ProgramData.Instance;

			string reportText = "Program Data Report:\r\n";
			reportText+= programData.ProgramCategoryList.Count + " Categories found\r\n";
			for (int c = 0; c < programData.ProgramCategoryList.Count; c++) {
				reportText += (c+1).ToString() + ": Category:" + programData.ProgramCategoryList [c].Name + " File:" + programData.ProgramCategoryList [c].File + "\r\n";
				reportText += " Programs:" + programData.ProgramListArray [c].Count + "\r\n";
				for (int p = 0; p < programData.ProgramListArray [c].Count; p++) {
					reportText += " " + (p+1).ToString() + ": Program:" + programData.ProgramListArray [c] [p].Name + " Steps:" + programData.ProgramListArray [c] [p].Steps.Count + "\r\n";
				}
			}

			return new SaveAndLoadText ();

			/*
			return new ContentPage { 
				Content = new Label {
					Text = "Hello, Forms!",
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
				},
			}; */
			/*if(m_RootGridPage == null)
			{
				m_RootGridPage = new RootGridPage ();
			};
			return m_RootGridPage;*/
		}
	}
}

