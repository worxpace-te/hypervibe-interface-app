﻿using System;

namespace HypervibeInterfaceApp
{
	public class Step
	{
		public string Phase { get; set; }
		public string Exercise { get; set; }
		public int Frequency { get; set; }
		public int Amplitude { get; set; }
		public string Feet { get; set; }
		public string Duration { get; set; }
		public string Rest { get; set; }
		public string Video { get; set; }

		public Step ()
		{
		}
	}
}

