﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Labs.Services;
using Xamarin.Forms.Labs;
using System.ComponentModel;

namespace HypervibeInterfaceApp
{
	public class UpperGForceReadout : RelativeLayout
	{
		private Label m_UpperLabel;
		private Label m_NumberLabel;
		private Label m_GLabel;

		private const int m_UpperLabelFontSize = 32;
		private const int m_NumberLabelFontSize = 56;
		private const int m_GLabelFontSize = 42;
		private readonly int m_NumberGDifference;

		private GForceViewModel m_GForce = new GForceViewModel();
		public string GForce { 
			get {
				return m_GForce != null ? m_GForce.GForce : "0.0";
			} 
			set {
				m_GForce.GForce = value;
			}
		}

		public UpperGForceReadout (string upperLabelText)
		{
			var device = Resolver.Resolve<IDevice> ();
			var display = device.Display;
			var cellWidth = display.ScreenWidthInches () / 3.0;

			GForce = "1.0";

			m_UpperLabel = new GothamBoldLabel (m_UpperLabelFontSize);
			m_UpperLabel.Text = upperLabelText;
			m_UpperLabel.WidthRequest = display.WidthRequestInInches (cellWidth);
			m_UpperLabel.XAlign = TextAlignment.Center;
			m_UpperLabel.YAlign = TextAlignment.End;
			m_UpperLabel.TextColor = Color.White;
			m_NumberLabel = new VitesseSansBoldLabel (m_NumberLabelFontSize);
			m_NumberLabel.HorizontalOptions = LayoutOptions.CenterAndExpand;
			m_NumberLabel.Text = "0.0";
			m_NumberLabel.BindingContext = m_GForce;
			m_NumberLabel.SetBinding (Label.TextProperty, "GForce");
			m_NumberLabel.TextColor = Color.White;
			m_GLabel = new VitesseSansLightLabel (m_GLabelFontSize);
			m_GLabel.Text = "G";
			m_GLabel.TextColor = Color.White;

			m_NumberGDifference = m_NumberLabelFontSize - m_GLabelFontSize;

			this.BackgroundColor = AppColors.Grey54;

			this.Children.Add (m_UpperLabel, Constraint.Constant(0), Constraint.Constant(20));

			// So that we can center the grouping of the number and G lablels, we place them into a relative
			// layout to position the G label relative to the number label, then we place that into a 
			// content view which can center it's content in the parent relative view..... Phew!
			var lowerLayout = new RelativeLayout {
				HorizontalOptions = LayoutOptions.Center
			};
			lowerLayout.Children.Add (m_NumberLabel, Constraint.Constant (0), Constraint.Constant (0));
			lowerLayout.Children.Add (m_GLabel, Constraint.RelativeToView (m_NumberLabel, (parent, view) => {
				return view.X + view.Width;
			}), Constraint.RelativeToView (m_NumberLabel, (parent, view) => {
				// We subtract an extra one to deal with a slight offset
				return view.Y + m_NumberGDifference - 1;
			}));
			var contentLayout = new ContentView {
				WidthRequest = display.WidthRequestInInches (cellWidth)
			};
			contentLayout.Content = lowerLayout;
			this.Children.Add (contentLayout, Constraint.Constant (0), Constraint.RelativeToView(m_UpperLabel, (parent, view) => {
				// 10 seems to be a good spacing amount between the items, matches with the given specs.
				return view.Height + 10;
			}));
		}
	}
}