﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Labs.Services;
using Xamarin.Forms.Labs;

namespace HypervibeInterfaceApp
{
	public class ButtonTextLayout : AbsoluteLayout
	{
		private Label[] m_Labels;
		private const int LabelCount = 4;
		private double[] m_Positions;

		public ButtonTextLayout()
		{
			var device = Resolver.Resolve<IDevice> ();
			var display = device.Display;

			m_Labels = new Label[LabelCount];
			m_Positions = new double[LabelCount];
			var spacing = display.ScreenWidthInches () / 10.0;
			m_Positions [0] = 0.0;
			m_Positions [1] = display.WidthRequestInInches(spacing * 2);
			m_Positions [2] = display.WidthRequestInInches(spacing * 4);
			m_Positions [3] = display.WidthRequestInInches(spacing * 6);

			for (int i = 0; i < LabelCount; i++) {
				m_Labels [i] = new GothamBookCondensedLabel (28);

				m_Labels [i].Text = "PROGRAMS";
				m_Labels [i].TextColor = Color.White;
				m_Labels [i].XAlign = TextAlignment.Center;
				m_Labels [i].YAlign = TextAlignment.Center;

				m_Labels [i].WidthRequest = display.WidthRequestInInches(spacing * 2);
				if (i == 3)
					m_Labels [i].WidthRequest = display.WidthRequestInInches(spacing * 4);
				m_Labels [i].HeightRequest = 50;

				this.Children.Add (m_Labels [i], new Point(m_Positions[i], 0));
			}

			this.BackgroundColor = AppColors.Grey54;
			this.Padding = 0;
		}

		public void SetButton(int index, string text, Color color)
		{
			if (index < 0 || index >= LabelCount) {
				throw new IndexOutOfRangeException("Index out of range.  " + LabelCount + " labels exist. " + index + " index given.");
			}
			m_Labels[index].TextColor = color;
			m_Labels[index].Text = text;
		}
	}
}

