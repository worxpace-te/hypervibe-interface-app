﻿using System;
using Xamarin.Forms;

namespace HypervibeInterfaceApp
{
	public static class AppColors
	{
		public static Color Grey54 { get { return Color.FromRgb(54, 54, 54); } }
		public static Color LightBlue { get { return Color.FromRgb(82, 154, 216); } } 
		public static Color LightGreen { get { return Color.FromRgb(121, 206, 66); } }
		public static Color Orange { get { return Color.FromRgb(236, 161, 58); } }
	}
}

