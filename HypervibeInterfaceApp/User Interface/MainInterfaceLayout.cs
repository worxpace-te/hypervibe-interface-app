﻿using System;
using Xamarin.Forms;

namespace HypervibeInterfaceApp
{
	public class MainInterfaceLayout : Grid
	{
		private UpperGForceReadout m_LoReadout;
		private UpperGForceReadout m_MedReadout;
		private UpperGForceReadout m_HiReadout;

		public MainInterfaceLayout ()
		{
			this.VerticalOptions = LayoutOptions.FillAndExpand;
			this.HorizontalOptions = LayoutOptions.FillAndExpand;
			this.Padding = 0;
			this.ColumnSpacing = 0;
			this.RowSpacing = 0;

			RowDefinitions.Add (new RowDefinition { Height = new GridLength (140, GridUnitType.Absolute) });
			RowDefinitions.Add (new RowDefinition { Height = new GridLength (50, GridUnitType.Absolute) });
			RowDefinitions.Add (new RowDefinition { Height = new GridLength (1, GridUnitType.Star) });

			ColumnDefinitions.Add (new ColumnDefinition { Width = new GridLength (1, GridUnitType.Star) });
			ColumnDefinitions.Add (new ColumnDefinition { Width = new GridLength (1, GridUnitType.Star) });
			ColumnDefinitions.Add (new ColumnDefinition { Width = new GridLength (1, GridUnitType.Star) });

			m_LoReadout = new UpperGForceReadout ("LO");
			m_LoReadout.BackgroundColor = AppColors.LightBlue;
			m_LoReadout.GForce = "1.7";
			m_MedReadout = new UpperGForceReadout ("MED");
			m_MedReadout.BackgroundColor = AppColors.LightGreen;
			m_MedReadout.GForce = "3.6";
			m_HiReadout = new UpperGForceReadout ("HI");
			m_HiReadout.BackgroundColor = AppColors.Orange;
			m_HiReadout.GForce = "7.2";
			this.Children.Add (m_LoReadout, 0, 1, 0, 1);
			this.Children.Add (m_MedReadout, 1, 2, 0, 1);
			this.Children.Add (m_HiReadout, 2, 3, 0, 1);

			this.BackgroundColor = Color.White;
		}
	}
}

