﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Labs;
using Xamarin.Forms.Labs.Services;
using Xamarin.Forms.Labs.Controls;

namespace HypervibeInterfaceApp
{
	public class RootGridPage : ContentPage
	{
		public event Action PowerButtonPressedEvent;
		public event Action ProgramsButtonPressedEvent;
		public event Action RightButtonPressedEvent;
		public event Action DownButtonPressedEvent;
		public event Action UpButtonPressedEvent;

		public RootGridPage()
		{
			var device = Resolver.Resolve<IDevice> ();
			var display = device.Display;
			var buttonSize = display.WidthRequestInInches(display.ScreenWidthInches () / 5.0);

			// Three rows = Main Content View, Button Text View, Buttons
			Grid grid = new Grid {
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				RowDefinitions = {
					new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
					new RowDefinition { Height = new GridLength(50, GridUnitType.Absolute) },
					new RowDefinition { Height = new GridLength(buttonSize, GridUnitType.Absolute) }
				},
				ColumnDefinitions = {
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
				},
				Padding = 0,
				ColumnSpacing = 0,
				RowSpacing = 0,
			};

			PowerButtonPressedEvent += () => System.Diagnostics.Debug.WriteLine("Power Button Pressed");
			ProgramsButtonPressedEvent += () => System.Diagnostics.Debug.WriteLine("Programs Button Pressed");
			RightButtonPressedEvent += () => System.Diagnostics.Debug.WriteLine("Right Button Pressed");
			DownButtonPressedEvent += () => System.Diagnostics.Debug.WriteLine("Down Button Pressed");
			UpButtonPressedEvent += () => System.Diagnostics.Debug.WriteLine("Up Button Pressed");

			// Main Section
			grid.Children.Add (new MainInterfaceLayout(), 0, 5, 0, 1);

			// Text Row
			var textRow = new ButtonTextLayout ();
			grid.Children.Add (textRow, 0, 5, 1, 2);
				
			// Button cells
			for(int i = 0; i < 5; i++) 
			{
				var img = new Image {
					Source = "button_program_disabled.png"
				};
				var gesture = new TapGestureRecognizer ();
				switch (i) {
				case 0: // Power
					gesture.Tapped += (sender, e) => {
						if (PowerButtonPressedEvent != null)
							PowerButtonPressedEvent ();
						MessagingCenter.Send<RootGridPage>(this, "PowerButtonPressed");
					};
					break;
				case 1: // Program
					gesture.Tapped += (sender, e) => {
						if (ProgramsButtonPressedEvent != null)
							ProgramsButtonPressedEvent ();
					};
					break;
				case 2: // Right
					gesture.Tapped += (sender, e) => {
						if (RightButtonPressedEvent != null)
							RightButtonPressedEvent ();
					};
					break;
				case 3: // Down
					gesture.Tapped += (sender, e) => {
						if (DownButtonPressedEvent != null)
							DownButtonPressedEvent ();
					};
					break;
				case 4: // Up
					gesture.Tapped += (sender, e) => {
						if (UpButtonPressedEvent != null)
							UpButtonPressedEvent ();
					};
					break;
				}

				img.GestureRecognizers.Add (gesture);
				
				grid.Children.Add (img, i, 2);
			}

			this.Padding = 0;

			this.Content = grid;
		}
	}
}

