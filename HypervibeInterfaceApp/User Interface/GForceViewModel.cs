﻿using System;
using System.ComponentModel;

namespace HypervibeInterfaceApp
{
	public class GForceViewModel : INotifyPropertyChanged
	{
		private string m_GForce = "0.0";
		public string GForce { 
			get {
				return m_GForce;
			}
			set {
				m_GForce = value;
				if (PropertyChanged != null) {
					PropertyChanged(this, new PropertyChangedEventArgs("GForce"));
				}
			}
		}

		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion


	}
}

