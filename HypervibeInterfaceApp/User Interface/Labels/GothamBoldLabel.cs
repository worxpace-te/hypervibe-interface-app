﻿using System;
using Xamarin.Forms;

namespace HypervibeInterfaceApp
{
	public class GothamBoldLabel : Label
	{
		public GothamBoldLabel (int size)
		{
			this.Font = Device.OnPlatform<Font>(
				Font.OfSize("GothamHTF-Bold.otf", size),
				Font.SystemFontOfSize(size),
				Font.SystemFontOfSize(size)
			);
		}
	}
}

