﻿using System;
using Xamarin.Forms;

namespace HypervibeInterfaceApp
{
	public class GothamBookLabel : Label
	{
		public GothamBookLabel(int size)
		{
			this.Font = Device.OnPlatform<Font>(
				Font.OfSize("GothamHTF-Book.otf", size),
				Font.SystemFontOfSize(size),
				Font.SystemFontOfSize(size)
			);
		}
	}
}

