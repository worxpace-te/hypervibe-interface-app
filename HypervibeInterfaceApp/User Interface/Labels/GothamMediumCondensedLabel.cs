﻿using System;
using Xamarin.Forms;

namespace HypervibeInterfaceApp
{
	public class GothamMediumCondensedLabel : Label
	{
		public GothamMediumCondensedLabel (int size)
		{
			this.Font = Device.OnPlatform<Font>(
				Font.OfSize("GothamHTF-MediumCondensed.otf", size),
				Font.SystemFontOfSize(size),
				Font.SystemFontOfSize(size)
			);
		}
	}
}

