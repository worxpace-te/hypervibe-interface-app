﻿using System;
using Xamarin.Forms;

namespace HypervibeInterfaceApp
{
	public class VitesseSansBoldLabel : Label
	{
		public VitesseSansBoldLabel (int size)
		{
			this.Font = Device.OnPlatform<Font>(
				Font.OfSize("VitesseSans-Bold.otf", size),
				Font.OfSize("VitesseSans-Bold.otf", size),
				Font.SystemFontOfSize(size)
			);
		}
	}
}

