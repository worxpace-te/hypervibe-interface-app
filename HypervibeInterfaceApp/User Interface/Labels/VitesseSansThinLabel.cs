﻿using System;
using Xamarin.Forms;

namespace HypervibeInterfaceApp
{
	public class VitesseSansThinLabel : Label
	{
		public VitesseSansThinLabel (int size)
		{
			this.Font = Device.OnPlatform<Font>(
				Font.OfSize("VitesseSans-Thin.otf", size),
				Font.SystemFontOfSize(size),
				Font.SystemFontOfSize(size)
			);
		}
	}
}

