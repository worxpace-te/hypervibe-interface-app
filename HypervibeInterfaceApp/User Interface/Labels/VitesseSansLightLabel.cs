﻿using System;
using Xamarin.Forms;

namespace HypervibeInterfaceApp
{
	public class VitesseSansLightLabel : Label
	{
		public VitesseSansLightLabel (int size)
		{
			this.Font = Device.OnPlatform<Font>(
				Font.OfSize("VitesseSans-Light.otf", size),
				Font.SystemFontOfSize(size),
				Font.SystemFontOfSize(size)
			);
		}
	}
}

