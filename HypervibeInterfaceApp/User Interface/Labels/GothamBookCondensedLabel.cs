﻿using System;
using Xamarin.Forms;

namespace HypervibeInterfaceApp
{
	public class GothamBookCondensedLabel : Label
	{
		public GothamBookCondensedLabel (int size)
		{
			this.Font = Device.OnPlatform<Font>(
				Font.OfSize("GothamHTF-BookCondensed.otf", size),
				Font.SystemFontOfSize(size),
				Font.SystemFontOfSize(size)
			);
		}
	}
}

