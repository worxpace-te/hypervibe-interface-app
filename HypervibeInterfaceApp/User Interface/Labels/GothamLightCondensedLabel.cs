﻿using System;
using Xamarin.Forms;

namespace HypervibeInterfaceApp
{
	public class GothamLightCondensedLabel : Label
	{
		public GothamLightCondensedLabel (int size)
		{
			this.Font = Device.OnPlatform<Font>(
				Font.OfSize("GothamHTF-LightCondensed.otf", size),
				Font.SystemFontOfSize(size),
				Font.SystemFontOfSize(size)
			);
		}
	}
}