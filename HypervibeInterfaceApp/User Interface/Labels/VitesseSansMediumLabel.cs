﻿using System;
using Xamarin.Forms;

namespace HypervibeInterfaceApp
{
	public class VitesseSansMediumLabel : Label 
	{
		public VitesseSansMediumLabel(int size)
		{
			this.Font = Device.OnPlatform<Font>(
				Font.OfSize("VitesseSans-Medium.otf", size),
				Font.SystemFontOfSize(size),
				Font.SystemFontOfSize(size)
			);
		}
	}
}
