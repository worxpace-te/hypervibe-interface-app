﻿using System;
using System.Threading.Tasks;

namespace HypervibeInterfaceApp
{
	/// <summary>
	/// Define an API for local storage file operations. Reference this interface
	/// in the common code, and implement this interface in the app projects for
	/// iOS, Android and WinPhone. Remember to use the 
	///     [assembly: Dependency (typeof (LocalStorage_IMPLEMENTATION_CLASSNAME))]
	/// attribute on each of the implementations.
	/// </summary>
	public interface ILocalStorage
	{
		Task SaveTextAsync (string filename, string text);
		Task<string> LoadTextAsync (string filename);
	}
}

