﻿using System;

namespace HypervibeInterfaceApp
{
	public static class Debug
	{
		private static Action<string> m_logAction = null;
		public static bool IsInit { get { return m_logAction != null; } }

		public static void Log (string logText) {
			m_logAction (logText);
		}

		public static void Init (Action<string> logAction) {
			m_logAction = logAction;
		}

	}
}

