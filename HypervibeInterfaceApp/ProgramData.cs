﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

namespace HypervibeInterfaceApp
{
	public class ProgramData
	{
		public List<Category> ProgramCategoryList { get; set; }
		public List<Program>[] ProgramListArray { get; set; }

		private static ProgramData m_Instance;

		public static ProgramData Instance 
		{
			get {
				if (m_Instance == null) { 
					m_Instance = new ProgramData ();
				}
				return m_Instance;
			}
		}

		private ProgramData ()
		{
			// debug all of the resources - in this case we just want to look at all of the file and make sure the xlsx files are there
			var assembly = typeof(ProgramData).GetTypeInfo().Assembly;
			foreach (var res in assembly.GetManifestResourceNames()) 
				Debug.Log("found resource: " + res);

			ProgramCategoryList = ReadCategoriesFromCSV ("HypervibeInterfaceApp.ProgramData.Categories.xlsx");

			ProgramListArray = new List<Program>[ProgramCategoryList.Count];

			for (int i = 0; i < ProgramCategoryList.Count; i++) {
				ProgramListArray [i] = ReadProgramFromCSV ("HypervibeInterfaceApp.ProgramData." + ProgramCategoryList[i].File);
			}

		}

		private List<Program> ReadProgramFromCSV( string filename) {
			List<Program> programList = new List<Program> ();
			var assembly = typeof(ProgramData).GetTypeInfo().Assembly;
			using (var iostream = assembly.GetManifestResourceStream (filename)) {
				// TODO: add null check and alert on iostream
				var workbook = new XlsxReader (iostream);
				foreach (var sheetName in workbook.WorksheetNames) {
					Program program = new Program ();
					program.Name = sheetName;
					var sheet = workbook [sheetName];
					int e = 0;
					foreach (var row in sheet.Entities<Step>()) {
						program.Steps.Add (row);
						e++;
						//Debug.Log ("Phase:" + row.Phase + " Exercise:" + row.Exercise + " Freq:" + row.Frequency + " Amp:" + row.Amplitude + " Feet:" + row.Feet + " Duration:" + row.Duration + " Rest:" + row.Rest + " Video:" + row.Video);
					}
					programList.Add (program);
					Debug.Log ("Program Name:" + program.Name + "Steps:" + e );
				}
			}
			// TODO: add error checking
			return programList;
		}

		private List<Category> ReadCategoriesFromCSV( string filename) {

			List<Category> programCategories = new List<Category> ();

			var assembly = typeof(ProgramData).GetTypeInfo().Assembly;
			using (var iostream = assembly.GetManifestResourceStream (filename)) {
				var workbook = new XlsxReader (iostream);
				foreach (var sheetName in workbook.WorksheetNames) {
					var sheet = workbook [sheetName];
					Debug.Log ("SHEET:" + sheetName);
					if (sheetName.CompareTo ("Hypervibe Categories") == 0) {
						Debug.Log ("Found: Hypervibe Categories - reading categories");
						foreach (var row in sheet.Entities<Category>()) {
							programCategories.Add (row);
							Debug.Log ("Category:" + row.Name + " File:" + row.File);
						}
					} else {
						//TODO: Add error handling if Hypervibe Categories is not found
					}
				}
			}
			// TODO: add error checking
			return programCategories;
		}
	}
}

