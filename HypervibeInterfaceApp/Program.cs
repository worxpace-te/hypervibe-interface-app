﻿using System;
using System.Collections.Generic;

namespace HypervibeInterfaceApp
{
	public class Program
	{
		public string Name { get; set; }
		public List<Step> Steps { get; set; }

		public Program ()
		{
			Steps = new List<Step> ();
		}
	}
}

