﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.Labs.Services;
using Xamarin.Forms.Labs.Droid;
using Xamarin.Forms.Labs;

namespace HypervibeInterfaceApp.Android
{
	[Activity (Label = "HypervibeInterfaceApp.Android.Android", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : XFormsApplicationDroid
	{
		private static bool m_ResolverSet = false;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			if(!m_ResolverSet) {
				var resolverContainer = new SimpleContainer ();
				resolverContainer.Register<IDevice> (t => AndroidDevice.CurrentDevice);
				Resolver.SetResolver (resolverContainer.GetResolver ());
			}

			Xamarin.Forms.Forms.Init (this, bundle);
			if (!Debug.IsInit) {
				Debug.Init ((string log) => {
					Console.WriteLine (log);
				});
			}

			SetPage (App.GetMainPage ());
		}
	}
}

