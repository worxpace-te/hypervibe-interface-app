﻿using System;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(HypervibeInterfaceApp.GothamBookLabel), typeof(HypervibeInterfaceApp.Android.GothamBookLabelRenderer))]
namespace HypervibeInterfaceApp.Android
{
	public class GothamBookLabelRenderer : CustomLabelRenderer
	{
		public override string FontName {
			get {
				return "GothamHTF-Book.otf";
			}
		}
	}
}

