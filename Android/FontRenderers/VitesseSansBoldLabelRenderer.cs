﻿using System;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(HypervibeInterfaceApp.VitesseSansBoldLabel), typeof(HypervibeInterfaceApp.Android.VitesseSansBoldLabelRenderer))]
namespace HypervibeInterfaceApp.Android
{
	public class VitesseSansBoldLabelRenderer : CustomLabelRenderer
	{
		public override string FontName {
			get {
				return "VitesseSans-Bold.otf";
			}
		}
	}
}

