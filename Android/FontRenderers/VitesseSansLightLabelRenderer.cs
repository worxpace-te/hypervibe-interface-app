﻿using System;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(HypervibeInterfaceApp.VitesseSansLightLabel), typeof(HypervibeInterfaceApp.Android.VitesseSansLightLabelRenderer))]
namespace HypervibeInterfaceApp.Android
{
	public class VitesseSansLightLabelRenderer : CustomLabelRenderer
	{
		public override string FontName {
			get {
				return "VitesseSans-Light.otf";
			}
		}
	}
}

