﻿using System;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(HypervibeInterfaceApp.VitesseSansThinLabel), typeof(HypervibeInterfaceApp.Android.VitesseSansThinLabelRenderer))]
namespace HypervibeInterfaceApp.Android
{
	public class VitesseSansThinLabelRenderer : CustomLabelRenderer
	{
		public override string FontName {
			get {
				return "VitesseSans-Thin.otf";
			}
		}
	}
}

