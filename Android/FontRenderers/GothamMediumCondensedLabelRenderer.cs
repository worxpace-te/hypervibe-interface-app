﻿using System;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(HypervibeInterfaceApp.GothamMediumCondensedLabel), typeof(HypervibeInterfaceApp.Android.GothamMediumCondensedLabelRenderer))]
namespace HypervibeInterfaceApp.Android
{
	public class GothamMediumCondensedLabelRenderer : CustomLabelRenderer
	{
		public override string FontName {
			get {
				return "GothamHTF-MediumCondensed.otf";
			}
		}
	}
}

