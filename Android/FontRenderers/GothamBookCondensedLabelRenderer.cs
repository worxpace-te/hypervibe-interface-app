﻿using System;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(HypervibeInterfaceApp.GothamBookCondensedLabel), typeof(HypervibeInterfaceApp.Android.GothamBookCondensedLabelRenderer))]
namespace HypervibeInterfaceApp.Android
{
	public class GothamBookCondensedLabelRenderer : CustomLabelRenderer
	{
		public override string FontName {
			get {
				return "GothamHTF-BookCondensed.otf";
			}
		}
	}
}

