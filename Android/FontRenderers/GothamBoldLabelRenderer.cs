﻿using System;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(HypervibeInterfaceApp.GothamBoldLabel), typeof(HypervibeInterfaceApp.Android.GothamBoldLabelRenderer))]
namespace HypervibeInterfaceApp.Android
{
	public class GothamBoldLabelRenderer : CustomLabelRenderer
	{
		public override string FontName {
			get {
				return "GothamHTF-Bold.otf";
			}
		}
	}
}

