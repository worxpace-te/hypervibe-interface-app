﻿using System;
using Xamarin.Forms.Platform.Android;
using Android.Widget;
using Android.Graphics;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(HypervibeInterfaceApp.VitesseSansMediumLabel), typeof(HypervibeInterfaceApp.Android.VitesseSansMediumLabelRenderer))]
namespace HypervibeInterfaceApp.Android
{
	public class VitesseSansMediumLabelRenderer : CustomLabelRenderer
	{
		public override string FontName {
			get {
				return "VitesseSans-Medium.otf";
			}
		}
	}
}

