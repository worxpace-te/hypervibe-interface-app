﻿using System;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.Widget;
using Android.Graphics;

namespace HypervibeInterfaceApp.Android
{
	public abstract class CustomLabelRenderer : LabelRenderer
	{
		public abstract string FontName { get; }

		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.Label> e)
		{
			base.OnElementChanged (e);
			var label = (TextView)Control;
			Typeface font = Typeface.CreateFromAsset (Forms.Context.Assets, FontName);
			label.Typeface = font;
		}
	}
}

