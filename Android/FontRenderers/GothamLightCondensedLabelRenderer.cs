﻿using System;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(HypervibeInterfaceApp.GothamLightCondensedLabel), typeof(HypervibeInterfaceApp.Android.GothamLightCondensedLabelRenderer))]
namespace HypervibeInterfaceApp.Android
{
	public class GothamLightCondensedLabelRenderer : CustomLabelRenderer
	{
		public override string FontName {
			get {
				return "GothamHTF-LightCondensed.otf";
			}
		}
	}
}

