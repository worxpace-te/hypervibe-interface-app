﻿using System;
using Xamarin.Forms;
using HypervibeInterfaceApp.Android;
using System.IO;
using System.Threading.Tasks;

[assembly: Dependency (typeof (LocalStorage_Android))]

namespace HypervibeInterfaceApp.Android
{
	public class LocalStorage_Android : ILocalStorage
	{
		#region ILocalStorage implementation

		public async Task SaveTextAsync (string filename, string text)
		{
			var docsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			var path = Path.Combine(docsPath, filename);

			using (StreamWriter sw = File.CreateText(path))
			{
				await sw.WriteAsync(text);
			}
		}

		public async Task<string> LoadTextAsync (string filename)
		{
			string text;
			var docsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			var path = Path.Combine(docsPath, filename);

			using (StreamReader sr = File.OpenText(path))
			{
				text = await sr.ReadToEndAsync();
			}
			return text;
		}

		#endregion
	}
}

