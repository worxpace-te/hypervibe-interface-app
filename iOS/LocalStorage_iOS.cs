﻿using System;
using Xamarin.Forms;
using HypervibeInterfaceApp.iOS;
using System.IO;
using System.Threading.Tasks;

[assembly: Dependency (typeof (LocalStorage_iOS))]

namespace HypervibeInterfaceApp.iOS
{
	public class LocalStorage_iOS : ILocalStorage
	{
		#region ILocalStorage implementation

		public async Task SaveTextAsync (string filename, string text)
		{
			using (StreamWriter sw = File.CreateText(filename))
			{
				await sw.WriteAsync(text);
			}
		}

		public async Task<string> LoadTextAsync (string filename)
		{
			string text;

			using (StreamReader sr = File.OpenText(filename))
			{
				text = await sr.ReadToEndAsync();
			}
			return text;
		}

		#endregion
	}
}

